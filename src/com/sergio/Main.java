package com.sergio;

import com.sergio.model.*;
import com.sergio.model.*;

import java.util.Objects;


public class Main {

    static final int organizationCount = 7;


    public static void main(String[] args) {
        // write your code here
        Finance[] organizations = new Finance[organizationCount];

        Currency[] currenciesExchanger = {new Currency("USD", 27.5F, 27.0F),
                new Currency("EUR", 31.0F, 30.0F)};

        organizations[0] = new Exchanger("Обменка 1", "Харьков 1", currenciesExchanger);
        organizations[1] = new Lombard("Ломбард 2", "Харьков 2");
        organizations[2] = new CreditCafe("Кредит-кафе 3", "Харьков 3");
        organizations[3] = new CreditAlliance("Кредитный союз 4", "Харьков 4");
        organizations[4] = new PIF("Кредитный союз 4", "Харьков 4", 2000);
        organizations[5] = new Post("Почта 5", "Харьков 5");

        Currency[] currenciesBank = {new Currency("USD", 27.0F, 26.5F),
                new Currency("EUR", 30.0F, 29.0F),
                new Currency("RUB", 0.45F, 0.46F)};

        organizations[6] = new Bank("МегаБанк 6", "Харьков 6", currenciesBank);

//-------------------------------------------------------

        printBestExchangeRate(organizations, 20_000F, "USD");
        printBestExchangeRate(organizations, 20_000F, "EUR");
        printBestExchangeRate(organizations, 20_000F, "RUB");


/*
        float maxExcUSD = 0;
        Finance bestExch = new Finance("", "");


        for (Finance organization : organizations) {

            if (organization instanceof Convert) {

                if ((((Convert) organization).sellCurrency(20_000, "USD") > 0) & (maxExcUSD == 0)) {
                    maxExcUSD = ((Convert) organization).sellCurrency(20_000, "USD");
                    bestExch = organization;
                    continue;
                }
                if ((((Convert) organization).sellCurrency(20_000, "USD") > maxExcUSD) & (maxExcUSD > 0)) {
                    maxExcUSD = ((Convert) organization).sellCurrency(20_000, "USD");
                    bestExch = organization;
                }
            }
        }

        System.out.println("Currency :" + maxExcUSD + "USD");
        System.out.println("Organization is:" + bestExch.getName() + " " + bestExch.getAddress());
        System.out.println("Max sum to convert is:" + ((Convert) bestExch).getMaxSumToConvert());
*/
    }
/*
    static void printBestCreditRate(Finance[] organizations, float money, float creditPercent) {


        boolean minPercent = false;
        Finance bestPercent = new Finance("", "");


        for (Finance organization : organizations) {

            if (organization instanceof Convert) {

                if ((((Credit) organization).getCredit(money, creditPercent) ) & (maxExcUSD == 0)) {
                    maxExcUSD = ((Convert) organization).sellCurrency(20_000, currencyName);
                    bestExch = organization;
                    continue;
                }

                if ((((Convert) organization).sellCurrency(20_000, currencyName) > maxExcUSD) & (maxExcUSD > 0F)) {
                    maxExcUSD = ((Convert) organization).sellCurrency(20_000, currencyName);
                    bestExch = organization;
                }
            }
        }
        if (!bestExch.getName().equalsIgnoreCase("") ){
            System.out.println("\n");
            System.out.println("Currency :" + maxExcUSD + currencyName);
            System.out.println("Organization is:" + bestExch.getName() + " " + bestExch.getAddress());
            System.out.println("Max sum to convert is:" + ((Convert) bestExch).getMaxSumToConvert());
        }

    }
*/

    static void printBestExchangeRate(Finance[] organizations, float currency, String currencyName) {


        float maxExcUSD = 0F;
        Finance bestExch = new Finance("", "");


        for (Finance organization : organizations) {

            if (organization instanceof Convert) {

                if ((((Convert) organization).sellCurrency(currency, currencyName) > 0) & (maxExcUSD == 0)) {
                    maxExcUSD = ((Convert) organization).sellCurrency(currency, currencyName);
                    bestExch = organization;
                    continue;
                }

                if ((((Convert) organization).sellCurrency(currency, currencyName) > maxExcUSD) & (maxExcUSD > 0F)) {
                    maxExcUSD = ((Convert) organization).sellCurrency(currency, currencyName);
                    bestExch = organization;
                }
            }
        }
        if (!bestExch.getName().equalsIgnoreCase("") ){
            System.out.println("\n");
            System.out.println("Currency :" + maxExcUSD + currencyName);
            System.out.println("Organization is:" + bestExch.getName() + " " + bestExch.getAddress());
            System.out.println("Max sum to convert is:" + ((Convert) bestExch).getMaxSumToConvert());
        }

    }

}


       /* Finance[] orgExchange = new Finance[organizationCount];

        int index = 0;
        for (Finance organization : organizations) {

            if (organization instanceof Convert) {
                if (((Convert) organization).getMaxSumToConvert() <= 20_000) {
                    orgExchange[index] = organization;
                    index++;
                }
            }


            float[] exchangeRateUSD = new float[organizationCount];
            float[] exchangeRateEUR = new float[organizationCount];
            float[] exchangeRateRUB = new float[organizationCount];


            float minExchangeRate=0;

            for (int i = 0; i < orgExchange.length; i++) {


                exchangeRateUSD[i] = ((Exchenger) orgExchange[i]).sellCurrency(20_000, "USD");
                exchangeRateEUR[i] = ((Exchenger) orgExchange[i]).sellCurrency(20_000, "EUR");
                exchangeRateRUB[i] = ((Exchenger) orgExchange[i]).sellCurrency(20_000, "RUB");
            }




        }


    }

    float findMinValue(float[] money) {

        float min = 0F;

        exit:
        for (int i = 0; i < money.length; i++) {
            if (money[i] != 0) {
                min = money[i];
                break exit;
            }
        }


        for (int i = 0; i < money.length; i++) {
            if ((money[i] < min) & (money[i] != 0)) {

                min = money[i];

            }

        }
        return  min;

    }*/
