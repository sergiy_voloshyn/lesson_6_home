package com.sergio.model;

public  class Finance {
    String name;
    String address;

    public String getName(){
        return  this.name;
    }

    public String getAddress() {
        return address;
    }

    public Finance(String name, String address) {
        this.name = name;
        this.address = address;
    }
}
