package com.sergio.model;

public interface Convert {


      float buyCurrency(float currency, String currencyName);

      float sellCurrency(float currency, String currencyName);

      float getMaxSumToConvert();


}
