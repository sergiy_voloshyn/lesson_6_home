package com.sergio.model;

/**
 * Created by user on 06.12.2017.
 */
public interface Deposit  {

    int getMinMonthCount();
    int getMaxMonthCount();
    float getDepositPercent();
    boolean getDeposit(int minMonth, int maxMonth);
}
